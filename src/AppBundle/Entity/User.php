<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Money", mappedBy="user")
     */
    private $pw;


    /**
     * Set pw
     *
     * @param \AppBundle\Entity\Money $pw
     *
     * @return User
     */
    public function setPw(\AppBundle\Entity\Money $pw = null)
    {
        $this->pw = $pw;

        return $this;
    }

    /**
     * Get pw
     *
     * @return \AppBundle\Entity\Money
     */
    public function getPw()
    {
        return $this->pw;
    }
}
