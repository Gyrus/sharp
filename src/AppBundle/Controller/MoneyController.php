<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Money;
use AppBundle\Form\AppMoneyType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Money controller.
 *
 * @Route("profile/money")
 */
class MoneyController extends Controller
{
    /**
     * Lists all money entities.
     *
     * @Route("/", name="profile_money_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $money = $em->getRepository('AppBundle:Money')->findOneBy(['user' => $user]);

        return $this->render('@App/Money/index.html.twig', array(
            'money' => $money,
        ));
    }

    /**
     * Finds and displays a money entity.
     *
     * @Route("/{id}", name="profile_money_show")
     * @Method("GET")
     */
    public function showAction(Money $money)
    {

        return $this->render('@App/Money/show.html.twig', array(
            'money' => $money,
        ));
    }

    /**
     * Finds and transfer money to another user
     *
     * @Route("/transfer/{id}", name="profile_money_transfer")
     * @Method("GET|POST")
     */
    public function transferAction(Request $request)
    {
        $money = new Money();
        $form = $this->createForm(AppMoneyType::class, $money);
        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $userRepository = $em->getRepository('AppBundle:User');
            $moneyRepository = $em->getRepository('AppBundle:Money');

            $userFrom = $this->getUser();
            $userTo = $userRepository->find($data->getUser());

            if($userFrom->getPw()->getPw() > $data->getPw()){
                $userFromMoney = $userFrom->getPw()->getPw() - $data->getPw();
                $userToMoney = $userTo->getPw()->getPw() + $data->getPw();
                $moneyTo = $moneyRepository->findOneBy(['user' => $userTo]);
                $moneyFrom = $moneyRepository->findOneBy(['user' => $userFrom]);
                $m1 = $moneyTo->setPw($userToMoney);
                $m2 = $moneyFrom->setPw($userFromMoney);
                $em->persist($m1);
                $em->persist($m2);
                $em->flush();
            }else{
                throw new \Exception('Not enought money');
            }
            return $this->redirectToRoute('profile_money_index');
        }

        return $this->render('AppBundle:Money:transfer.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
