<?php
namespace AppBundle\DataFixtures;


use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class UserFixture extends Fixture implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('admin@sharp.loc');

        $encore = $this->container->get('security.password_encoder');
        $password = $encore->encodePassword($user,'password');
        $user->setPassword($password);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEnabled(1);

        $manager->persist($user);
        $manager->flush();
    }

}