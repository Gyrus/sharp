<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav nav-pills nav-fill');
        $menu->addChild('index', array('route' => 'indexPage'))
            ->setAttributes(['class' => 'nav-item'])
            ->setLinkAttributes(['class' => 'nav-link']
            );
        $auth_checker = $this->container->get('security.authorization_checker');
        $token = $this->container->get('security.token_storage')->getToken();
        if($token->getUser() != 'anon.' ){
            if($auth_checker->isGranted('ROLE_ADMIN')){
                $menu->addChild('Administration', array('route' => 'admin'))
                    ->setAttributes(['class' => 'nav-item'])
                    ->setLinkAttributes(['class' => 'nav-link']
                    );
            }elseif($auth_checker->isGranted('ROLE_USER')){
                $menu->addChild('Profile', array('route' => 'fos_user_profile_show'))
                    ->setAttributes(['class' => 'nav-item'])
                    ->setLinkAttributes(['class' => 'nav-link']
                    );
                $menu->addChild('Money', array('route' => 'profile_money_index'))
                    ->setAttributes(['class' => 'nav-item'])
                    ->setLinkAttributes(['class' => 'nav-link']
                    );
            }
        }else{
            $menu->addChild('Login', array('route' => 'fos_user_security_login'))
                ->setAttributes(['class' => 'nav-item'])
                ->setLinkAttributes(['class' => 'nav-link']
                );
        }
        if($token->getUser() && $auth_checker->isGranted('ROLE_USER')) {
            $menu->addChild('Log Out', array('route' => 'fos_user_security_logout'))
                ->setAttributes(['class' => 'nav-item'])
                ->setLinkAttributes(
                    ['class' => 'nav-link']
                );
        }else{
            $menu->addChild('Registration', array('route' => 'fos_user_registration_register'))
                ->setAttributes(['class' => 'nav-item'])
                ->setLinkAttributes(
                    ['class' => 'nav-link']
                );
        }

// access services from the container!
        $em = $this->container->get('doctrine')->getManager();
// findMostRecent and Blog are just imaginary examples
//$blog = $em->getRepository('AppBundle:Blog')->findMostRecent();

//$menu->addChild('Latest Blog Post', array(
//'route' => 'blog_show',
//'routeParameters' => array('id' => $blog->getId())
//));

// create another menu item
//$menu->addChild('About Me', array('route' => 'about'));
// you can also add sub level's to your menu's as follows
//$menu['About Me']->addChild('Edit profile', array('route' => 'edit_profile'));

// ... add more children

        return $menu;
    }
}