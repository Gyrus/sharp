<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180209022813 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE money (id INT AUTO_INCREMENT NOT NULL, pw INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE money_user (money_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_621F754EBF29332C (money_id), INDEX IDX_621F754EA76ED395 (user_id), PRIMARY KEY(money_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE money_user ADD CONSTRAINT FK_621F754EBF29332C FOREIGN KEY (money_id) REFERENCES money (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE money_user ADD CONSTRAINT FK_621F754EA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE money_user DROP FOREIGN KEY FK_621F754EBF29332C');
        $this->addSql('DROP TABLE money');
        $this->addSql('DROP TABLE money_user');
    }
}
