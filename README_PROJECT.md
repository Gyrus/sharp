Project info
========================

Project use:

  * [**Composer**][1]  - Dependency Manager for PHP
  * [**MigrationBundle**][2] - The database migrations
  * [**FosUserBundle**][3] - The component wich allow to have great functionality of user
  * [**yarn**][4] - Used to install all dependencies for a project.
  * [**KnpMenuBundle**][5] - Used to create menu.
  
To start project you must have composer and yarn, than follow next commands:

    1. composer install
    2. yarn install
    3. yarn run encore dev (for production use prod instead dev)  

[1]: https://getcomposer.org/
[2]: https://symfony.com/doc/master/bundles/DoctrineMigrationsBundle/index.html#content_wrapper
[3]: https://symfony.com/doc/current/bundles/FOSUserBundle/index.html
[4]: https://yarnpkg.com/en/docs/install
[5]: http://symfony.com/doc/master/bundles/KnpMenuBundle/index.html